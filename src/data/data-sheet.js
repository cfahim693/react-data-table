const list =[
    {
        name:'Legit',
        rating:'8.2',
        year:'2013'
    },
    {
        name:'Veep',
        rating:'8.3',
        year:'2012'
    },

    {
        name:'Wilfred',
        rating:'7.8',
        year:'2011'
    },
    {
        name:'Workaholics',
        rating:'8.1',
        year:'2011'
    },
    {
        name:"Bob's Burger",
        rating:'8.1',
        year:'2011'
    },

    {
        name:'Louie',
        rating:'8.5',
        year:'2010'
    },
    {
        name:'Modern Family',
        rating:'8.4',
        year:'2009'
    },
    {
        name:'Community',
        rating:'8.5',
        year:'2009'
    },
    {
        name:'Parks and Recreation',
        rating:'8.6',
        year:'2009'
    },
    {
        name:'30 Rock',
        rating:'8.2',
        year:'2006'
    },
    {
        name:'Lucky Louie',
        rating:'7.9',
        year:'2006'
    },
    {
        name:'The Big Bang Theory',
        rating:'8.1',
        year:'2007'
    },
    {
        name:'How I Met Your Mother',
        rating:'8.3',
        year:'2005'
    },
    {
        name:"It's Always Sunny in Philadelphia",
        rating:'8.8',
        year:'2005'
    },
    {
        name:'Extras',
        rating:'8.3',
        year:'2005'
    },
    {
        name:'The Office',
        rating:'8.8',
        year:'2005'
    },
    {
        name:'Freaks and Geeks',
        rating:'8.8',
        year:'1999'
    },
    {
        name:'Family Guy',
        rating:'8.1',
        year:'1999'
    },
    {
        name:'Friends',
        rating:'8.9',
        year:'1994'
    },
    {
        name:'The Simpsons',
        rating:'8.7',
        year:'1989'
    },
    {
        name:'Seinfeld',
        rating:'8.8',
        year:'1989'
    },

    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },

    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    // {
    //     name:'Legit',
    //     rating:'8.2',
    //     year:'2013'
    // },
    
    
]

export default list;