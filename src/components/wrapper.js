import React from 'react';
import AppBar from './app-bar'
import DataTable from './data-table'

function Wrapper() {
  return (
    <div>
      <AppBar/>
      <DataTable/>
    </div>
  );
}

export default Wrapper;
