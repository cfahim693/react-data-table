import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import List from '../data/data-sheet'
import YearPicker from "react-year-picker"
import { Button } from '@material-ui/core';

const columns = [
    { id: 'name', label: 'Name' },
    { id: 'rating', label: 'IMDB Rating' },
    {
        id: 'year',
        label: 'Year of Release',
    },
];

const rows = List;

const useStyles = makeStyles({
    root: {
        width: '100%',
        marginTop: '20px'
    },
    tableWrapper: {
        maxHeight: 407,
        overflow: 'auto',
    },
});

export default function StickyHeadTable() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [data, setData] = React.useState(rows)
    const filterYear = []

    function handleChangePage(event, newPage) {
        setPage(newPage);
    }

    function handleChangeRowsPerPage(event) {
        setRowsPerPage(+event.target.value);
        setPage(0);
    }

    function handleChange(date) {
        // console.log(date)
        const str_date = JSON.stringify(date)
        console.log(str_date)
        rows.map(item => {
            if (item.year === str_date) {
                filterYear.push(item)
            }
        })
        setData(filterYear)

    }

    function handleAll() {
        setData(rows)
    }

    return (
        <React.Fragment>
            <div style={{ marginTop: '20px', marginLeft: '10px', display: "inline-block" }}>
                <YearPicker onChange={handleChange} />

            </div>
            <div style={{ display: "inline-block",textAlign:"right" }}>
                <Button color="primary" onClick={handleAll}>Show All</Button>
            </div>
            <Paper className={classes.root}>
                <div>
                    <Table >
                        <TableHead>
                            <TableRow>
                                {columns.map(column => (
                                    <TableCell
                                        key={column.id}
                                    >
                                        {column.label}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                        {columns.map(column => {
                                            const value = row[column.id];
                                            return (
                                                <TableCell key={column.id} >
                                                    {column.format && typeof value === 'number' ? column.format(value) : value}
                                                </TableCell>
                                            );
                                        })}
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'previous page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'next page',
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </React.Fragment>

    );
}